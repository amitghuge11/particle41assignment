# Particle41Assignment
Particle41 Assignment Android
Assignment consist of four screens:
1) Landing Page
2) Category List Page
3) Category Images Page
4) Details Page
Getting Started
Clone and open project in Android Studio.
Prerequisites
Android Studio v3.+
Java V1.7+
Installing
Follow steps to start with the application
1) Open Project in Android Studio
2) Let the Studio get done with importing dependencies and indexing of the project.
3) Create build by going into Build->Build APK
4) Install the build on the Android Enabled Device
Running the tests
Follow the steps mentioned below:
1) Check if user sees all the categories when he launches the application for the first time.
2) Click the category and check the images of clicked category index
3) Come back on landing page and check if user sees maximum times clicked category.
4) Repeat the steps for multiple categories and observe the result.
Built With
* Android Studio 
* Gradle 
* JAVA 
License
This project is licensed under the MIT License - see the LICENSE.md file for details


