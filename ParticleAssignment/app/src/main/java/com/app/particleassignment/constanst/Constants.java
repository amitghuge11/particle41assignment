package com.app.particleassignment.constanst;

public class Constants {
    // Table Name
    public static final String TABLE_NAME = "UserClick";

    // Table columns
    public static final String _ID = "_id";
    public static final String CLICK = "clicks";
    public static final String CATEGORY = "category";

    // Database Information
    public static final String DB_NAME = "User_Interests.DB";

    // database version
    public static final int DB_VERSION = 1;
}
