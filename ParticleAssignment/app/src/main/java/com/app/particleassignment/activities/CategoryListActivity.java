package com.app.particleassignment.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.app.particleassignment.R;
import com.app.particleassignment.adapters.CategoryApadter;
import com.app.particleassignment.constanst.Constants;
import com.app.particleassignment.db_utils.DBManager;
import com.app.particleassignment.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryListActivity extends AppCompatActivity {

    private GridView mGridCategories;
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Category List");

        initializeViews();
        registerListeners();
    }

    private void registerListeners() {
        mGridCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(CategoryListActivity.this, CategoryResultsActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        showCategoryData();
    }

    private void showCategoryData() {
        List<CategoryModel> categoriesList = new ArrayList<>();
        dbManager = new DBManager(this);
        dbManager.open();
        Cursor cursor = dbManager.getDataByOrderDescending();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                CategoryModel model = new CategoryModel();
                model.setCatId(cursor.getInt(cursor.getColumnIndex(Constants._ID)));
                model.setClicks(cursor.getInt(cursor.getColumnIndex(Constants.CLICK)));
                model.setCatName(cursor.getString(cursor.getColumnIndex(Constants.CATEGORY)));
                categoriesList.add(model);
                cursor.moveToNext();
            }
        }

        CategoryApadter adapter = new CategoryApadter(this, categoriesList);
        mGridCategories.setAdapter(adapter);
    }

    private void initializeViews() {
        mGridCategories = findViewById(R.id.gridview_categories);
    }
}
