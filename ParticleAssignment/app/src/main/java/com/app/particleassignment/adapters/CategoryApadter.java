package com.app.particleassignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.app.particleassignment.R;
import com.app.particleassignment.activities.CategoryResultsActivity;
import com.app.particleassignment.db_utils.DBManager;
import com.app.particleassignment.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryApadter extends BaseAdapter {
    private Context context;
    private List<CategoryModel> mListCategories;
    private LayoutInflater inflter;

    public CategoryApadter(Context applicationContext, List<CategoryModel> categories) {
        this.context = applicationContext;
        mListCategories = new ArrayList<>();
        this.mListCategories.addAll(categories);
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return mListCategories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.layout_category_grid_item, null);

        final CategoryModel model = mListCategories.get(i);
        Button categoryName = (Button) view.findViewById(R.id.button_category);
        categoryName.setText(model.getCatName());

        categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBManager dbManager = new DBManager(context);
                dbManager.open();
                int clicksCount = model.getClicks() + 1;
                dbManager.updateCount(model.getCatId(), clicksCount);
                context.startActivity(new Intent(context, CategoryResultsActivity.class).putExtra("category", model.getCatId()));
            }
        });

        return view;
    }
}
