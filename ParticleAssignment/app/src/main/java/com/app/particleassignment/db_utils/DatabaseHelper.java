package com.app.particleassignment.db_utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.app.particleassignment.constanst.Constants.CATEGORY;
import static com.app.particleassignment.constanst.Constants.CLICK;
import static com.app.particleassignment.constanst.Constants.DB_NAME;
import static com.app.particleassignment.constanst.Constants.DB_VERSION;
import static com.app.particleassignment.constanst.Constants.TABLE_NAME;
import static com.app.particleassignment.constanst.Constants._ID;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CLICK + " INTEGER, " + CATEGORY + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
