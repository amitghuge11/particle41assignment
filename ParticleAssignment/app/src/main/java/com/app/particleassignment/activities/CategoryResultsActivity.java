package com.app.particleassignment.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.particleassignment.R;
import com.app.particleassignment.adapters.ImageResultArrayAdapter;
import com.app.particleassignment.models.ImageResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class CategoryResultsActivity extends AppCompatActivity {

    private ArrayList<ImageResult> mListImageResults = null;
    private ImageResultArrayAdapter mImageAdapter;
    private GridView mGridViewImageResult;
    private TextView mTextTag;
    private int mCatID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_results);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (getIntent().getExtras() != null) {
            mCatID = getIntent().getIntExtra("category", 1);
        }
        getSupportActionBar().setTitle("Category " + mCatID);
        initializeViews();
        searchImages();
        registerListeners();
        mTextTag.setText("Showing result for category " + mCatID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void registerListeners() {
        mGridViewImageResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(CategoryResultsActivity.this, ImageDetailsActivity.class).putExtra("url", mListImageResults.get(position).getFullUrl()));
            }
        });
    }

    private void initializeViews() {
        mGridViewImageResult = findViewById(R.id.gridview_category_images);
        mTextTag = findViewById(R.id.tv_category);
    }

    public void searchImages() {
        Toast.makeText(this, "Searching for images", Toast.LENGTH_SHORT)
                .show();
        AsyncHttpClient client = new AsyncHttpClient();

        String url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyAG90SxSQ-uRsYKKXJSDvyEVXo8_vkCook&cx=013055762706877564122:9xywvsjvshq&q=" + mCatID + "&alt=json";
        client.get(url, null,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        JSONArray imageJsonResults = null;
                        try {
                            imageJsonResults = response.getJSONArray("items");
                            mListImageResults = new ArrayList<ImageResult>();
                            mListImageResults.addAll(ImageResult
                                    .fromJSONArray(imageJsonResults));
                            mImageAdapter = new ImageResultArrayAdapter(CategoryResultsActivity.this, mListImageResults);
                            mGridViewImageResult.setAdapter(mImageAdapter);
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Log.d("DEBUG", errorResponse.toString());
                    }
                }
        );
    }
}
