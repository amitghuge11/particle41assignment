package com.app.particleassignment.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.app.particleassignment.R;
import com.app.particleassignment.adapters.CategoryApadter;
import com.app.particleassignment.constanst.Constants;
import com.app.particleassignment.db_utils.DBManager;
import com.app.particleassignment.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class LandingPageActivity extends AppCompatActivity {

    private GridView mGridCategories;
    private Button mButtonGoToListing;
    private TextView mTextScreenMessage;
    private DBManager dbManager;
    private String[] categories = {"Category1", "Category2", "Category3", "Category4", "Category5", "Category6"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        initializeViews();
        registerListeners();
    }

    private void registerListeners() {

        mGridCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(LandingPageActivity.this, CategoryResultsActivity.class));
            }
        });

        mButtonGoToListing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingPageActivity.this, CategoryListActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showCategoryData();
    }

    private void showCategoryData() {
        dbManager = new DBManager(this);
        dbManager.open();
        Cursor cursor = dbManager.fetch(false);
        List<CategoryModel> categoriesList = new ArrayList<>();

        if (cursor.getCount() > 0) {
            mTextScreenMessage.setText(getString(R.string.string_interested_in));
            setDataFromDatabase(true);
        } else {
            mTextScreenMessage.setText(getString(R.string.string_welcome_message));
            for (String category : categories) {
                CategoryModel model = new CategoryModel();
                model.setCatName(category);
                model.setClicks(0);
                categoriesList.add(model);
            }
            dbManager.insert(categoriesList);
            setDataFromDatabase(false);
        }
    }

    private void setDataFromDatabase(boolean isMaxValue) {
        List<CategoryModel> categoriesList = new ArrayList<>();
        Cursor cursor = dbManager.fetch(isMaxValue);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                CategoryModel model = new CategoryModel();
                model.setCatId(cursor.getInt(cursor.getColumnIndex(Constants._ID)));
                model.setClicks(cursor.getInt(cursor.getColumnIndex(Constants.CLICK)));
                model.setCatName(cursor.getString(cursor.getColumnIndex(Constants.CATEGORY)));
                categoriesList.add(model);
                cursor.moveToNext();
            }
        }

        CategoryApadter adapter = new CategoryApadter(this, categoriesList);
        mGridCategories.setAdapter(adapter);
    }

    private void initializeViews() {
        mTextScreenMessage = findViewById(R.id.text_message);
        mGridCategories = findViewById(R.id.gridview_categories);
        mButtonGoToListing = findViewById(R.id.button_listings);
    }
}
