package com.app.particleassignment.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.app.particleassignment.R;
import com.loopj.android.image.SmartImageView;

public class ImageDetailsActivity extends AppCompatActivity {
    private SmartImageView imageViewDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Full Category Image");

        initializeViews();
        String imageURL = null;
        if (getIntent().getExtras() != null) {
            imageURL = getIntent().getExtras().getString("url");
        }
        imageViewDetails.setImageUrl(imageURL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void initializeViews() {
        imageViewDetails = findViewById(R.id.image_result);
    }
}
