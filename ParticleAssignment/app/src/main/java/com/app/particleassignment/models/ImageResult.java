package com.app.particleassignment.models;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

public class ImageResult implements Serializable {
    private String fullUrl;
    private String thumbUrl;

    public ImageResult() {
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String toString() {
        return this.thumbUrl;
    }

    public static ArrayList<ImageResult> fromJSONArray(JSONArray array) {

        ArrayList<ImageResult> results = new ArrayList<ImageResult>();
        for (int x = 0; x < array.length(); x++) {
            try {
                ImageResult result = new ImageResult();
                result.setFullUrl(array.getJSONObject(x).getJSONObject("pagemap").getJSONArray("cse_image").getJSONObject(0).getString("src"));
                result.setThumbUrl(array.getJSONObject(x).getJSONObject("pagemap").getJSONArray("cse_thumbnail").getJSONObject(0).getString("src"));
                results.add(result);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return results;
    }
}
