package com.app.particleassignment.db_utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.app.particleassignment.constanst.Constants;
import com.app.particleassignment.models.CategoryModel;

import java.util.List;

public class DBManager {

    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(List<CategoryModel> categories) {
        for (int i = 0; i < categories.size(); i++) {
            ContentValues contentValue = new ContentValues();
            contentValue.put(Constants.CATEGORY, categories.get(i).getCatName());
            contentValue.put(Constants.CLICK, 0);
            database.insert(Constants.TABLE_NAME, null, contentValue);
        }
    }

    public Cursor getDataByOrderDescending() {
        String[] columns = new String[]{Constants._ID, Constants.CATEGORY, Constants.CLICK};
        Cursor cursor = null;

        cursor = database.query(Constants.TABLE_NAME, columns, null, null, null, null, Constants.CLICK + " DESC");

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor fetch(boolean isMaxCondition) {
        Cursor cursor = null;
        if (!isMaxCondition) {
            cursor = getAllData();
        } else {
            cursor = database.rawQuery("select *,max(clicks) from UserClick ", null);
        }

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    private Cursor getAllData() {
        String[] columns = new String[]{Constants._ID, Constants.CATEGORY, Constants.CLICK};
        Cursor cursor = null;

        cursor = database.query(Constants.TABLE_NAME, columns, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int updateCount(int _id, int click) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.CLICK, click);
        int success = database.update(Constants.TABLE_NAME, contentValues, Constants._ID + " = " + _id, null);
        return success;
    }
}
